/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Zandut <mfauzan613110035@gmail.com>
 */
public class Database
{
    private Connection conn;
    private Statement sta;
    private String username = "root";
    private String url = "jdbc:mysql://localhost/db_mvc";

    public void connect()
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, "");
            sta = conn.createStatement();
            System.out.println("Berhasil !!!");
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    public int manipulate(String query)
    {
        try
        {
            return sta.executeUpdate(query);
        } catch (Exception e)
        {
            e.printStackTrace();
            return -1;
        }
    }
    
    public ResultSet get(String query)
    {
        try
        {
            return sta.executeQuery(query);
        } catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
    
    public void disconnect()
    {
        try
        {
            if (!conn.isClosed())
            {
                sta.close();
                conn.close();
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
    
    
    
}
